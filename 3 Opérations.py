# Opérateurs en python :    + (addition)
#                           - (soustraction)
#                           * (multiplication)
#                           / (division)
#                           % (modulo : reste de la division)

calcul = 5 / 2

#Entier type Integer

calcul_entier = int(calcul)
reste_entier = 5 % 2
print("Résultat = {} reste {}".format(calcul_entier, reste_entier))

#Flottant type Float

calcul_flottant = float(calcul)
calcul_flottant1 = 5.0 / 2.0
print("Résultat = {} ou encore {}".format(calcul_flottant, calcul_flottant1))

#Afficher résultat directement

print(calcul)

#Incrémentation

niveauPersonnage = 1
print("Personnage niveau ", niveauPersonnage)

niveauPersonnage += 1
# équivalent : niveauPersonnage = niveauPersonnage +1
print("Personnage niveau ", niveauPersonnage)