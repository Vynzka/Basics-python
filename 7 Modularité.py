# Exemple calcule d'une racine carré : besoin d'un module de mathématiques

import math
# from <nom_module> import <fonction>   On importe alors seulement la fonction du module
# from <nom_module> import *            Importe la totalité du module math

resultat = math.sqrt(9)     #fonction racine carré du module math
print(resultat)

sinus = math.sin(90)
print(sinus)

# On peu également créer soit même un module

import includes.ModulePerso as modPers      # Le point sert de séparateur pour le chemin du module
modPers.parler("Vynz", "Le codeur fou")
modPers.stop()