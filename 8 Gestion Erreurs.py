ageUser = input("Quel âge avez vous ? ")

# Si on entre une valeur autre que décimale, le programme plantera
ageUser = int(ageUser)
print("Vous avez bien", ageUser, "ans.")

# Pour y remédier :
try:                                                # On test alors le cast avec un Try
    ageUser = int(ageUser)
except:                                             # Dans le cas d'une erreur
    print("La valeur indiquée est incorrect.")
else:                                               # Pas obligatoire, peut être intégré dans le try
    print("Vous avez bien", ageUser, "ans.")
finally:                                            # Pas obligatoire
    print("Fin du programme...")

# Gérer plusieurs exceptions :

nombre1 = 150
nombre2 = input("Entrez un nombre à diviser : ")

try:
    nombre2 = int(nombre2)
    print("Résultat = {].".format(nombre1 / nombre2))
except ZeroDivisionError:
    print("Vous ne pouvez pas diviser par zéro")
except ValueError:
    print("Vous devez entrer un nombre")
else:
    print("Votre nombre est valide.")
finally:
    print("Fin du programme")

# Lever soit même une erreur avec le mot clé "raise"

age = input("Quel âge avez vous ? ")
age = int(age)

if age < 25:
    raise UnderAgeError     # L'erreur prend le nom qu'on lui donne.

# Une assertion est une fonction de vérification en quelques sortes
# Si l'âge indiqué est inférieur à 25, il y aura une erreur d'assertion.

try:
    age = input("Quel âge avez vous ? ")
    age = int(age)
    assert age > 25:    # Je veux que cette condition soit rempli. (sinon : exception erreur)
except AssertionError:
    print("J'ai attrapé l'exception !")