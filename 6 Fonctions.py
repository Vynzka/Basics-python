# Fonctions vues :
#   print(), input()
#   type(), int(), float(), str(), bool()

# Retour sur l'usage des fonctions vues

destinataire = input("Nom du destinataire : ")
message = input("Corps du message : ")

# Suite :

def dire(nom="Default", corps="Sans objet"):    # Défini un argument par défaut
    print("{} : {}".format(nom, corps))

dire(destinataire, message)

dire(message, destinataire)
dire(corps=message, nom=destinataire)   # affranchi l'ordre de saisi et s'écrira dans le bon sens

# On utilise une fonction quand une opération nécessite d'être répétée plusieurs fois

# Fonction avec un nombre infini d'arguments

def show_inventory(*list_item):
    for item in list_item:
        print(item)

show_inventory("épée", "casque", "cape", "bouclier")

# Fonction avec retour

def calculer_somme(nombre1, nombre2):
    resultat = nombre1 + nombre2
    return resultat                     # Le return fait automatiquement sortir de la fonction

a = input("Entre un premier nombre : ")
a = int(a)
b = input("Puis un deuxième : ")
b = int(b)

print(calculer_somme(a, b))

# Fonction lambda (dite anonyme) utile pour réaliser une seule tâche

prixTTC = lambda prixHT: prixHT + (prixHT * 20 / 100)
convertirPrix = input("Prix à convertir : ")
convertirPrix = int(convertirPrix)
print("Le prix TTC sera de : ", prixTTC(convertirPrix), "euros")