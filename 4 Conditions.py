# Opérateurs de comparaison :   == (égal à)
#                               != (différent de)
#                               <  (plus petit que)
#                               >  (plus grand que)
#                               <= (inférieur ou égal)
#                               >= (supérieur ou égal)

# Conditions multiples :    and (ET)
#                           or (OU)
#                           in / not in (DANS / PAS DANS)

# Mots clés des conditions : if / elif / else

identifiant1 = "Thomas"
mot_de_passe1 = "1234"

identifiant2 = "Pierre"
mot_de_passe2 = "6789"

print("Identification")
user_id = input("ID : ")
user_password = input("Password : ")

if (user_id == identifiant1 and user_password == mot_de_passe1) or (user_id == identifiant2 and user_password == mot_de_passe2):
    print("Authentification réussie, bienvenue", user_id)
else:
    print("Echec de l'authentification")

# Test de caractère
lettre = input("Entrez un caractère : ")
if lettre in "aeiouy":
    print("c'est une voyelle")
else:
    print("c'est une conçonne")

# Fourchette
age = input("Quel est votre âge : ")
age = int(age)
if age > 0 and age < 18:            # syntaxe lourde
    print("Vous êtes mineur")
elif 18 >= age <= 50:               # plus optimisé
    print("La force de l'âge !")
else:
    print("La retraite approche !")