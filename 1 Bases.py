print("Hello World !")

# Types standards : Entier numérique (int)
#                   Nombre flottant (float)
#                   Chaîne de caractères (str)
#                   Booléen (bool)

agePersonne = 23
agePersonne2 = "25"
prixHT = 99.99
continuer_partie = False

print("Age de la personne : ", agePersonne, "ans.")

texte = "La personne a {} ans et le prix HT est de {}€."
print(texte.format(agePersonne, prixHT))

