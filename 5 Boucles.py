# Boucle simple

compteur = 0

while compteur < 5:
    print("Je suis une boucle")
    compteur += 1

print("Fin de la boucle")

# ---------------------------------

# Boucles   : While / For
# Mots-clés : break (casse la boucle) / continue (revient au début de la boucle)

 # Boucle While -------------------


jeu_lance = True

while jeu_lance:
    # Différentes instructions
    choixJeu = input("Continue > ")

    if choixJeu == "Non":
        break
    elif choixJeu == "Oui":
        continue
    else:
        print("Répondez par Oui ou Non.")

print("A bientôt !")

# Boucle For ----------------------

phrase = "Hello World !"

for lettre in phrase:
    print(lettre)


# Exemple plus complexe :
# list_news = get_new()
# for news in list_news:
#   print ()