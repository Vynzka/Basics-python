class Humain:
    # Classe des êtres humains

    humains_crees = 0

    def __init__(self, classe_prenom="Défaut", classe_age=1):
        print("Création d'un humain...")
        self.prenom = classe_prenom
        self.age = classe_age
        Humain.humains_crees += 1
        # pass   : tu ne fais rien

print("Lancement du programme...")

H1 = Humain()
print("Prénom > {} âge > {} ans".format(H1.prenom, H1.age))

H2 = Humain("Robert", 45)

# Changer un attribut en dehors de la classe est possible en python
H1.prenom = "Thomas"
print("Prénom > {}".format(H1.prenom))

print("Humains créés : {}".format(Humain.humains_crees))