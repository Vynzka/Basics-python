# Introduction

# Le python est un langage tout objet, on utilise des classes pour manipuler les objets
# La POO est une manière d'organiser son code finalement (paradigme)

# ageUtilisateur = 14     : On créé un objet de classe int

# Classe : plan de conception, un genre (ex : Humain)
# Objet : instance de classe, du genre de la classe (ex: Julien)
# -----> Julien est un objet de classe Humain

# Attribut  : variable de classe (ex : prénom, age, sexe, taille, ...)
# Propriété : manière de manipuler les attributs (lecture seule, accès non autorisé en dehors de la classe, ...)

# Méthode           : fonction d'une classe (ex : manger(nourriture, quantite), marcher, parler, ...) ce qu'il peut faire
# Méthode de classe : fonction propre à la classe (explication par la suite avec application)
# Méthode statique  : fonction d'une classe, mais indépendante de celle-ci.

# Héritage  : classe Fille qui hérite d'une classe Mère (ex : Animal -> Panda, Ours, ...)
#           : classe Chat hérite de la classe Animal (Chat est une sorte d'Animal)

